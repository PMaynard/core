package main

import (
	"net/http"
	"net/url"
	"strings"
	"testing"
)

// ============================================================================================
// AUTH
// ============================================================================================
func TestAuthEmail(t *testing.T) {
	data := url.Values{
		"email": {"hello@example.org"},
	}
	req, _ := http.NewRequest("POST", "/auth/email", strings.NewReader(data.Encode()))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	validJSON(t, response.Body)
}

func TestAuthEmailVerify(t *testing.T) {
	data := url.Values{
		"code": {"xyz"},
	}
	req, _ := http.NewRequest("POST", "/auth/email/verify", strings.NewReader(data.Encode()))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	validJSON(t, response.Body)
}

func TestAuthRefresh(t *testing.T) {
	req, _ := http.NewRequest("GET", "/auth/refresh", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotImplemented, response.Code)
	validJSON(t, response.Body)
}
