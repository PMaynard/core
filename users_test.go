package main

import (
	"net/http"
	"testing"
)

// ============================================================================================
// User
// ============================================================================================
func TestUserMe(t *testing.T) {
	req, _ := http.NewRequest("GET", "/users/me", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	validJSON(t, response.Body)
}

func TestUserProfile(t *testing.T) {
	req, _ := http.NewRequest("GET", "/users/auser", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	validJSON(t, response.Body)
}

func TestUserUpdate(t *testing.T) {
	req, _ := http.NewRequest("PATCH", "/users/me", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotImplemented, response.Code)
	validJSON(t, response.Body)
}
