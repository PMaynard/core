package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/url"
	"strings"
	"testing"
)

// ============================================================================================
// Regression Tests
// ============================================================================================
func TestSeenFault0001(t *testing.T) {
	// TODO: This needs more looking into.
	// req, _ := http.NewRequest("GET", "/0/places?location='	data.location '&radius=50", nil)
	req, _ := http.NewRequest("GET", "/0/places?location='%20%20 data.location%20%20 '&radius=50", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
}

// ============================================================================================
// Functionality Tests
// ============================================================================================
func TestPlacesBadRequestNoQuieres(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/places", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
	validJSON(t, response.Body)
}

func TestPlacesLocation(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/places?location=-1.92+54.60", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	expStatus := 1
	expMessage := "Yo"
	expDataLen := 9
	expDataName := "Sunderland"

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}

	if len(m.Data) != expDataLen {
		t.Errorf("Expected a Data of length '%d'. Got '%d'", expDataLen, len(m.Data))
		return
	}

	if m.Data[4].Name != expDataName {
		t.Errorf("Expected a place name of '%s'. Got '%s'", expDataName, m.Data[4].Name)
	}
}

func TestPlacesLocationRadius(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/places?location=-5.92 54.60&radius=10000", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	expStatus := 1
	expMessage := "Yo"
	expDataLen := 1
	expDataName := "Belfast"

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}

	if len(m.Data) != expDataLen {
		t.Errorf("Expected a Data of length '%d'. Got '%d'", expDataLen, len(m.Data))
		return
	}

	if m.Data[0].Name != expDataName {
		t.Errorf("Expected a place name of '%s'. Got '%s'", expDataName, m.Data[0].Name)
	}
}

func TestPlacesLocationFilter(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/places?location=-6.266155+53.350140&filter=IE", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != 1 {
		t.Errorf("Expected a Status of 1. Got '%d'", m.Status)
	}

	if m.Message != "Yo" {
		t.Errorf("Expected a Message of 'Yo'. Got '%s'", m.Message)
	}

	if len(m.Data) != 1 {
		t.Errorf("Expected a Data of length '1'. Got '%d'", len(m.Data))
		return
	}

	if m.Data[0].Name != "Dublin" {
		t.Errorf("Expected a place name of 'Dublin'. Got '%s'", m.Data[0].Name)
	}
}

func TestPlacesLocationQuery(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/places?location=7.216236 51.481846&query=nd", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != 1 {
		t.Errorf("Expected a Status of 1. Got '%d'", m.Status)
	}

	if m.Message != "Yo" {
		t.Errorf("Expected a Message of 'Yo'. Got '%s'", m.Message)
	}

	if len(m.Data) != 2 {
		t.Errorf("Expected a Data of length '2'. Got '%d'", len(m.Data))
		return
	}

	if m.Data[0].Name != "Dortmund" {
		t.Errorf("Expected a place name of 'Dortmund'. Got '%s'", m.Data[0].Name)
	}

	if m.Data[1].Name != "Roermond" {
		t.Errorf("Expected a place name of 'Roermond'. Got '%s'", m.Data[1].Name)
	}
}

func TestPlacesLocationRadiusQueryFilter(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/places?location=7.216236 51.481846&radius=5000000&query=nd&filter=NL", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != 1 {
		t.Errorf("Expected a Status of 1. Got '%d'", m.Status)
	}

	if m.Message != "Yo" {
		t.Errorf("Expected a Message of 'Yo'. Got '%s'", m.Message)
	}

	if len(m.Data) != 1 {
		t.Errorf("Expected a Data of length '1'. Got '%d'", len(m.Data))
		return
	}

	if m.Data[0].Name != "Roermond" {
		t.Errorf("Expected a place name of 'Roermond'. Got '%s'", m.Data[0].Name)
	}
}

func TestPlace(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/place/11", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != 1 {
		t.Errorf("Expected a Status of 1. Got '%d'", m.Status)
	}

	if m.Message != "Yo" {
		t.Errorf("Expected a Message of 'Yo'. Got '%s'", m.Message)
	}

	if len(m.Data) != 1 {
		t.Errorf("Expected a Data of length '1'. Got '%d'", len(m.Data))
		return
	}

	if m.Data[0].PlaceID != 11 {
		t.Errorf("Expected a PlaceID of '11'. Got '%d'", m.Data[0].PlaceID)
	}

	if m.Data[0].Name != "Chester" {
		t.Errorf("Expected a place name of 'Chester'. Got '%s'", m.Data[0].Name)
	}
}

func TestPlace_Invalid(t *testing.T) {
	expStatus := 0
	expMessage := "Request Failed"

	req, _ := http.NewRequest("GET", "/0/place/0", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotFound, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestUpdatePlace_Name(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/place/11", nil)
	response := executeRequest(req)
	var placeOrig map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &placeOrig)

	payload := []byte(`{"name":"New PeteVile"}`)

	req, _ = http.NewRequest("PATCH", "/0/place/11", bytes.NewBuffer(payload))
	response = executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	expStatus := 1
	expMessage := "Place ID '11' Successfully Updated."

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestUpdatePlace_CountryCode(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/place/11", nil)
	response := executeRequest(req)
	var placeOrig map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &placeOrig)

	payload := []byte(`{"countrycode":"PM"}`)

	req, _ = http.NewRequest("PATCH", "/0/place/11", bytes.NewBuffer(payload))
	response = executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	expStatus := 1
	expMessage := "Place ID '11' Successfully Updated."

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestUpdatePlace_Location(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/place/11", nil)
	response := executeRequest(req)
	var placeOrig map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &placeOrig)

	payload := []byte(`{"location":"0 0"}`)

	req, _ = http.NewRequest("PATCH", "/0/place/11", bytes.NewBuffer(payload))
	response = executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	expStatus := 1
	expMessage := "Place ID '11' Successfully Updated."

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestUpdatePlace_LocationName(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/place/11", nil)
	response := executeRequest(req)
	var placeOrig map[string]interface{}
	json.Unmarshal(response.Body.Bytes(), &placeOrig)

	payload := []byte(`{"location":"-2.8908956 53.190887", "name": "Chester"}`)

	req, _ = http.NewRequest("PATCH", "/0/place/11", bytes.NewBuffer(payload))
	response = executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	expStatus := 1
	expMessage := "Place ID '11' Successfully Updated."

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestCreatePlace(t *testing.T) {
	expStatus := 1
	expMessage := "Successfully Added Place"
	expDataLen := 1
	expDataName := "Petes Fancy House"
	expDataLoc := "0 0"
	expDataCC := "GB"

	payload := []byte(`{"location":"` + expDataLoc + `", "name": "` + expDataName + `", "countrycode": "` + expDataCC + `"}`)

	req, _ := http.NewRequest("PUT", "/0/place", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusOK, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}

	if len(m.Data) != expDataLen {
		t.Errorf("Expected a Data of length '%d'. Got '%d'", expDataLen, len(m.Data))
		return
	}

	if m.Data[0].Name != expDataName {
		t.Errorf("Expected a place name of '%s'. Got '%s'", expDataName, m.Data[0].Name)
	}

	if m.Data[0].Location != expDataLoc {
		t.Errorf("Expected a location of '%s'. Got '%s'", expDataLoc, m.Data[0].Location)
	}

	if m.Data[0].CountryCode != expDataCC {
		t.Errorf("Expected a CountryCode of '%s'. Got '%s'", expDataCC, m.Data[0].CountryCode)
	}
}

func TestCreatePlace_Fail001(t *testing.T) {
	expStatus := 0
	expMessage := "Request Failed"
	expDataName := "Petes Fancy House"
	expDataLoc := "0 0"
	expDataCC := "GB"

	// Missing ("l) at the start of the payload
	payload := []byte(`{ocation":"` + expDataLoc + `", "name": "` + expDataName + `", "countrycode": "` + expDataCC + `"}`)

	req, _ := http.NewRequest("PUT", "/0/place", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestCreatePlace_Fail002(t *testing.T) {
	expStatus := 0
	expMessage := "Request Failed"
	expDataName := "Petes Fancy House"
	expDataLoc := "0 0"
	expDataCC := "GB"

	// Missing (l) at the start of the payload
	payload := []byte(`{"ocation":"` + expDataLoc + `", "name": "` + expDataName + `", "countrycode": "` + expDataCC + `"}`)

	req, _ := http.NewRequest("PUT", "/0/place", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestCreatePlace_Fail003(t *testing.T) {
	expStatus := 0
	expMessage := "Request Failed"
	expDataName := "Petes Fancy House"
	expDataLoc := "0 0"
	expDataCC := "GB"

	// Missing (n) at for name.
	payload := []byte(`{"location":"` + expDataLoc + `", "ame": "` + expDataName + `", "countrycode": "` + expDataCC + `"}`)

	req, _ := http.NewRequest("PUT", "/0/place", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestCreatePlace_Fail004(t *testing.T) {
	expStatus := 0
	expMessage := "Request Failed"
	expDataName := "Petes Fancy House"
	expDataLoc := "0 0"
	expDataCC := "GB"

	// Missing (c) at for CountryCode.
	payload := []byte(`{"location":"` + expDataLoc + `", "name": "` + expDataName + `", "ountrycode": "` + expDataCC + `"}`)

	req, _ := http.NewRequest("PUT", "/0/place", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestCreatePlace_Fail005(t *testing.T) {
	expStatus := 0
	expMessage := "Request Failed"
	expDataName := "1"
	expDataLoc := "0 0"
	expDataCC := "GB"

	// Invalid name
	payload := []byte(`{"location":"` + expDataLoc + `", "name": "` + expDataName + `", "countrycode": "` + expDataCC + `"}`)

	req, _ := http.NewRequest("PUT", "/0/place", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestCreatePlace_Fail006(t *testing.T) {
	expStatus := 0
	expMessage := "Request Failed"
	expDataName := "Petes Fancy House"
	expDataLoc := "0"
	expDataCC := "GB"

	// Invalid Location
	payload := []byte(`{"location":"` + expDataLoc + `", "name": "` + expDataName + `", "countrycode": "` + expDataCC + `"}`)

	req, _ := http.NewRequest("PUT", "/0/place", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestCreatePlace_Fail007(t *testing.T) {
	expStatus := 0
	expMessage := "Request Failed"
	expDataName := "Petes Fancy House"
	expDataLoc := "0"
	expDataCC := "G"

	// Invalid CC
	payload := []byte(`{"location":"` + expDataLoc + `", "name": "` + expDataName + `", "countrycode": "` + expDataCC + `"}`)

	req, _ := http.NewRequest("PUT", "/0/place", bytes.NewBuffer(payload))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusBadRequest, response.Code)
	resJSON := validJSON(t, response.Body)

	var m Response
	if err := json.Unmarshal(resJSON, &m); err != nil {
		t.Errorf("Error")
	}

	if m.Status != expStatus {
		t.Errorf("Expected a Status of '%d'. Got '%d'", expStatus, m.Status)
	}

	if m.Message != expMessage {
		t.Errorf("Expected a Message of '%s'. Got '%s'", expMessage, m.Message)
	}
}

func TestCreatePlaceReview(t *testing.T) {
	data := url.Values{
		"emotion": {"..."},
		"comment": {"good"},
	}
	req, _ := http.NewRequest("PUT", "/0/place/11/review", strings.NewReader(data.Encode()))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotImplemented, response.Code)
	validJSON(t, response.Body)
}

func TestEditPlaceReview(t *testing.T) {
	data := url.Values{
		"emotion": {"..."},
		"comment": {"good"},
	}
	req, _ := http.NewRequest("PATCH", "/0/place/11/review/2222", strings.NewReader(data.Encode()))
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotImplemented, response.Code)
	validJSON(t, response.Body)
}
