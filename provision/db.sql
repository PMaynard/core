DROP TABLE IF EXISTS places CASCADE;

CREATE TABLE IF NOT EXISTS places (
    PlaceID serial PRIMARY KEY,
    name text,
    countryCode text,
    location GEOGRAPHY(POINT,4326),
    location_str text
);
-- Index the test table with a spherical index
CREATE INDEX global_points_gix ON places USING GIST ( location );

-- https://postgis.net/docs/manual-2.5/using_postgis_dbmanagement.html#Geography_Basics

-- Distance query 100km tolerance, centered on Belfast
SELECT name FROM places WHERE ST_DWithin(location, 'SRID=4326;POINT(-5.92 54.60)'::geography, 100000);

-- -------------------
--  Londonderry/Derry
--  Armagh
--  Newry
--  Belfast
--  Lisburn
-- (5 rows)
