# Deploy

1. Download/Upload the new code base to ```/vagrant```
2. Make sure the server is configured the same:

	ansible-playbook -i inventory playbook.yml

3. Restart service

	service core restart

