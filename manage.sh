#!/usr/bin/bash

set -e

function log {
	echo -e "[-] $1 \n"
}

function run_tests {
	log "Running Tests";
	
	log "golint -set_exit_status"
	golint -set_exit_status
	
	log "go test -short"
	go test -short
	
	log "go test -race -short"
	go test -race -short

	log "go test -covermode=count -coverprofile \"core\" macak.co.uk/goappz/core"
	go test -covermode=count -coverprofile "core" macak.co.uk/goappz/core
}

function pre_commit {
	log "Running Pre Commit";
	go fmt
}

case $1 in
	test )
		run_tests
		log "Done"
		;;
	commit )
		run_tests
		pre_commit
		log "Done"
		;;
	force-refresh )
		log "go build && vagrant rsync"
		go build && vagrant rsync
		log "vagrant provision"
		vagrant provision
		;;
	refresh )
		log "go build && vagrant rsync && vagrant ssh -c 'sudo service core restart'"
		go build && vagrant rsync && vagrant ssh -c 'sudo service core restart'
		;;
	* )
		log "manage.sh [test | commit]"
		;;
esac