package main

import (
	"net/http"
	"testing"
)

func TestMedia(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/media/33?width=120", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotImplemented, response.Code)
	validJSON(t, response.Body)
}

func TestMediaUpload(t *testing.T) {
	req, _ := http.NewRequest("PUT", "/0/media", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotImplemented, response.Code)
	validJSON(t, response.Body)
}
