package main

func main() {
	c := Core{}
	c.init()
	c.run(c.Config.Listen, c.Config.ListenStats)
}
