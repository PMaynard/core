package main

import (
	"database/sql"
	"encoding/json"
	"github.com/getsentry/raven-go"
	"github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus/promhttp"
	// "github.com/rs/cors"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
)

// Response | Used to return to client.
type Response struct {
	Status  int    `json:status,int`
	Message string `json:message,string`
	Data    Places `json:data`
}

// Place | Used to store a place
type Place struct {
	PlaceID     int    `json:"placeid,int"`
	Name        string `json:name,string`
	CountryCode string `json:countryCode,string`
	Location    string `json:location,string`
}

// Places | Lots of places
type Places []Place

// Config | Core's configuration
type Config struct {
	Logfile       string `json:"logfile"`
	LogAccessfile string `json:"logaccessfile"`
	LogErrorfile  string `json:"logerrorfile"`
	Listen        string `json:"listen"`
	ListenStats   string `json:"listenStats"`

	DBHost    string
	DBUser    string
	DBPass    string
	DBName    string
	DBSSLMode string

	CorsAllowedOrigins []string
	CorsAllowedMethods []string
	CorsDebug          bool
}

// Core | Global place for core// things
type Core struct {
	Router    *mux.Router
	DB        *sql.DB
	Config    Config
	AccessLog *log.Logger
	ErrorLog  *log.Logger
}

func (c *Core) init() {
	c.loadConfig()
	c.setupLogging()
	c.initDB()
	c.Router = mux.NewRouter()
	c.initRoutes()
}

// Default Locations:
// - ENV: CORE_CONFIG
// - ./core.conf
// -  /etc/core-appz/core.conf
func (c *Core) loadConfig() {
	var conf Config

	f, err := os.Open(os.Getenv("CORE_CONFIG"))
	if err != nil {
		f, err = os.Open("./core.conf")
		if err != nil {
			f, err = os.Open("/etc/core-appz/core.conf")
			if err != nil {
				log.Fatal("Unable to load config (CORE_CONFIG | ./core.conf | /etc/core-appz/core.conf)")
			}
		}
	}
	defer f.Close()

	// TODO: This does not get included in the core.log file, since it is called before logging it setup.
	log.Print("Using config file: ", f.Name())

	byteValue, _ := ioutil.ReadAll(f)
	err = json.Unmarshal(byteValue, &conf)
	if err != nil {
		panic(err)
	}

	c.Config = conf
}

func (c *Core) run(addr string, metricsAddr string) {
	go func() {
		log.Print("Metrics Access on http://", metricsAddr, "/metrics")
		http.Handle("/metrics", promhttp.Handler())
		http.ListenAndServe(metricsAddr, nil)
	}()

	if raven.URL() != "" {
		log.Print("Sentry Enabled: ", raven.URL())
	} else {
		log.Print("Sentry Not Enabled")
	}

	// middilewareCors := cors.New(cors.Options{
	// 	AllowedOrigins: c.Config.CorsAllowedOrigins,
	// 	AllowedMethods: c.Config.CorsAllowedMethods,
	// 	Debug:          c.Config.CorsDebug,
	// })

	s := &http.Server{
		Addr: addr,
		// Handler: middilewareCors.Handler(c.Router),
		Handler: c.Router,
	}

	log.Fatal(s.ListenAndServe())
	log.Print("Listening and Serving on http://", addr)
}

func (c *Core) setupLogging() {
	// Core Logging: core.log and stdout
	log.SetFlags(log.Ldate | log.Ltime | log.Lshortfile)
	f, err := os.OpenFile(c.Config.Logfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}

	// TODO: Printed only to STD out.
	log.Print("Core Logging: ", f.Name())

	// TODO: I assume this losses std.out and std.err
	mw := io.MultiWriter(os.Stdout, f)
	log.SetOutput(mw)

	// Set up the HTTP log files

	// Access Log
	f, err = os.OpenFile(c.Config.LogAccessfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	c.AccessLog = log.New(f, "", 0)
	log.Print("HTTP Access Logging: ", f.Name())

	// Error Log
	f, err = os.OpenFile(c.Config.LogErrorfile, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		log.Fatalf("error opening file: %v", err)
	}
	c.ErrorLog = log.New(f, "", 0)
	log.Print("HTTP Error Logging: ", f.Name())

}

// TODO Function; Returns NotImplemented.
func (c *Core) todo(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusNotImplemented)
	// log.Printf("TODO Function: [%s %s %s]", r.Method, r.URL, r.Proto)
	if err := json.NewEncoder(w).Encode(Response{Status: 0, Message: "Working on it."}); err != nil {
		log.Fatal(err)
	}
}

// Catch All Error.
func (c *Core) sendInternalError(w http.ResponseWriter, r *http.Request, err error, statusCode int) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(statusCode)
	msg := "Request Failed"
	if err != nil {
		log.Printf("[ERROR] [%s %s %s] msg(%s) error(%s)", r.Method, r.URL, r.Proto, msg, err)
	} else {
		log.Printf("[ERROR] [%s %s %s] msg(%s) error(N/A)", r.Method, r.URL, r.Proto, msg)
	}
	if err := json.NewEncoder(w).Encode(Response{Status: 0, Message: msg}); err != nil {
		panic(err)
	}
}
