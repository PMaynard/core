// database
package main

import (
	"database/sql"
	"fmt"
	_ "github.com/lib/pq"
	"log"
)

func (c *Core) initDB() {
	var psqlInfo string
	var err error

	if len(c.Config.DBPass) > 1 {
		// If there is a password, we assume it will be over network
		psqlInfo = fmt.Sprintf("host=%s user=%s password=%s dbname=%s sslmode=%s", c.Config.DBHost, c.Config.DBUser, c.Config.DBPass, c.Config.DBName, c.Config.DBSSLMode)
	} else {
		// Otherwise we assume a unix socket
		psqlInfo = fmt.Sprintf("host=%s user=%s dbname=%s sslmode=%s", c.Config.DBHost, c.Config.DBUser, c.Config.DBName, c.Config.DBSSLMode)
	}

	log.Print("Connection String:", psqlInfo)

	c.DB, err = sql.Open("postgres", psqlInfo)
	if err != nil {
		log.Panic(err)
	}

	err = c.DB.Ping()
	if err != nil {
		log.Panic(err)
	}

	log.Print("Successfully connected to database!")
}
