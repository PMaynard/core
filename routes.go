package main

import "github.com/getsentry/raven-go"

// https://api.domain.com/v1/[app ID]/[namespace]/[endpoint]

func (c *Core) initRoutes() {
	c.Router.Use(c.HTTPLog)

	// AUTH
	c.Router.HandleFunc("/auth/email", raven.RecoveryHandler(c.email)).Methods("POST")
	c.Router.HandleFunc("/auth/email/verify", raven.RecoveryHandler(c.emailVerify)).Methods("POST")
	c.Router.HandleFunc("/auth/refresh", raven.RecoveryHandler(c.todo)).Methods("GET")

	// Config
	c.Router.HandleFunc("/{AppID:[0-9]+}/config", raven.RecoveryHandler(c.todo)).Methods("GET")

	// User
	c.Router.HandleFunc("/users/me", raven.RecoveryHandler(c.userMe)).Methods("GET")
	c.Router.HandleFunc("/users/{username}", raven.RecoveryHandler(c.userProfile)).Methods("GET")
	c.Router.HandleFunc("/users/me", raven.RecoveryHandler(c.todo)).Methods("PATCH")

	// Places
	c.Router.HandleFunc("/{AppID:[0-9]+}/places", raven.RecoveryHandler(c.places)).Methods("GET")

	c.Router.HandleFunc("/{AppID:[0-9]+}/place", raven.RecoveryHandler(c.createplace)).Methods("PUT")
	c.Router.HandleFunc("/{AppID:[0-9]+}/place/{ID:[0-9]+}", raven.RecoveryHandler(c.place)).Methods("GET")
	c.Router.HandleFunc("/{AppID:[0-9]+}/place/{ID:[0-9]+}", raven.RecoveryHandler(c.updateplace)).Methods("PATCH")
	// Places/Review
	c.Router.HandleFunc("/{AppID:[0-9]+}/place/{ID:[0-9]+}/review", raven.RecoveryHandler(c.todo)).Methods("PUT")
	c.Router.HandleFunc("/{AppID:[0-9]+}/place/{ID:[0-9]+}/review/{ReviewID:[0-9]+}", raven.RecoveryHandler(c.todo)).Methods("PATCH")

	// Media
	c.Router.HandleFunc("/{AppID:[0-9]+}/media/{MediaID:[0-9]+}", raven.RecoveryHandler(c.todo)).Methods("GET")
	c.Router.HandleFunc("/{AppID:[0-9]+}/media", raven.RecoveryHandler(c.todo)).Methods("PUT")
}
