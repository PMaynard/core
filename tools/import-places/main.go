// Import Places

package main


import (
"encoding/json"
  "database/sql"
  "fmt"
  "os"

  _ "github.com/lib/pq"
)

const (
  host     = "127.0.0.1"
  port     = 5432
  user     = "core"
  password = "easy"
  dbname   = "core"
)

type place struct {
  Name string `json:"name"`
  Lat float32 `json:"lat"`
  Lon float32 `json:"lon"`
  CountryCode string `json:"countryCode"`
}

type places []place

func readPlaces(fileToOpen string) (places) {
    data, err := os.Open(fileToOpen)
    if err != nil {
        panic(err)
    }

    var ps places

    jsonParser := json.NewDecoder(data)
    if err = jsonParser.Decode(&ps); err != nil {
        panic(err)
    }

    fmt.Printf("Read '%d' places from file '%s'\n", len(ps), fileToOpen)

    return ps
}

func insertIntoDB(db *sql.DB, p place) (int) {
	fmt.Printf("%+v\n", p)
	geo := fmt.Sprintln(p.Lon, p.Lat)

	id := 0
	err := db.QueryRow("INSERT INTO places (name, countryCode, location, location_str) VALUES ($1, $2, 'SRID=4326;POINT("+geo+")', $3) RETURNING PlaceID", p.Name, p.CountryCode, geo).Scan(&id)
	
	if err != nil {
		panic(err)
	}
	return id
}

func main() {

// Read all the palces
	places := readPlaces("../../data/cities-small.json")

// Connect to DB
  psqlInfo := fmt.Sprintf("host=%s port=%d user=%s "+
    "password=%s dbname=%s sslmode=disable",
    host, port, user, password, dbname)
  db, err := sql.Open("postgres", psqlInfo)
  if err != nil {
    panic(err)
  }
  defer db.Close()

  err = db.Ping()
  if err != nil {
    panic(err)
  }

  fmt.Println("Successfully connected!")

// Do the magic
	i := 0
	for i < len(places) {
		id := insertIntoDB(db, places[i])
		fmt.Println(id)
		i = i + 1
	}

}