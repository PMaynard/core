package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"log"
	"net/http"
)

// User ...
type User struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
}

// Users | lots of users
type Users []User

func (c *Core) userMe(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(Response{Status: 1, Message: "{ User{Id: 1111, Name: \"Zorg\"}"}); err != nil {
		log.Panic(err)
	}
}

func (c *Core) userProfile(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	vars := mux.Vars(r)
	if err := json.NewEncoder(w).Encode(Response{Status: 1, Message: vars["username"]}); err != nil {
		log.Panic(err)
	}
}
