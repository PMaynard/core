# CORE //

- Master branch accessible at: <https://core.port22.co.uk/>
- Using Go's Modules for dependency versioning.
	- This means you don't need to keep the code in ```~/go``` and no need to run ```go get```.
- `service core [ restart | start | stop ]` Core can be managed using systemd.
- `journalctl -fu core` is used to view core's output. (`lnav` can also be used)

# Build and run

	git clone
	go test
	go build
	./core

# Vagrant Development

Image is based on CentOS. You need ansible installed on your host machine.

	vagrant up

Run vagrant's script to keep the guest's /vagrant dir and host working directory in sync.

	vagrant rsync-auto

Running from Host machine, you will need to pass connection string to override the default UNIX socket connection. As well as specify a config file.

	CORE_CONFIG='core.conf.example' CORE_CONSTR='host=127.0.0.1 user=core password=easy sslmode=disable' go test 

Querying the database from the host. Using the forwarded ports

	PGPASSWORD="easy" psql -h 127.0.0.1 -U core

Exporting the database from Vagrant guest, to a location on the host machine.

	vagrant ssh -c PGPASSWORD="easy" pg_dump -h 127.0.0.1 -U core' > data/data.sql

Build, sync, restart; The circle of life. 

	go build && vagrant rsync && vagrant ssh -c 'sudo service core restart'

