# DATA

## Cities

Cities.json from OSM, thanks to [this](https://stackoverflow.com/questions/11484574/database-of-world-cities-open-street-map)

- <http://www.overpass-api.de/query_form.html>

        [out:json];
        node
          ["place"="city"];
        out body;

### Process Cities

Extract some bits we want:

	jq '.elements[]' cities.json | jq '{id: .id, lat: .lat, lon: .lon, name: .tags.name, countryCode: .tags."is_in:country_code" }' | jq -s '.' > cities-small.json

