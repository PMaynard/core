package main

import (
	"encoding/json"
	"github.com/gorilla/mux"
	"html"
	"log"
	"net/http"
)

func (c *Core) email(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(Response{Status: 1, Message: html.EscapeString(r.FormValue("email"))}); err != nil {
		log.Panic(err)
	}
}

func (c *Core) emailVerify(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)
	vars := mux.Vars(r)

	if err := json.NewEncoder(w).Encode(Response{Status: 1, Message: html.EscapeString(vars["code"])}); err != nil {
		log.Panic(err)
	}
}
