package main

import (
	"bytes"
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"os"
	"testing"
)

var a Core

func TestMain(m *testing.M) {
	a = Core{}
	a.init()

	code := m.Run()

	os.Exit(code)
}

// ============================================================================================
// Config
// ============================================================================================
func TestConfig(t *testing.T) {
	req, _ := http.NewRequest("GET", "/0/config", nil)
	response := executeRequest(req)
	checkResponseCode(t, http.StatusNotImplemented, response.Code)
	validJSON(t, response.Body)
}

// ============================================================================================
// Misc functions for testing.
// ============================================================================================
func executeRequest(req *http.Request) *httptest.ResponseRecorder {
	rr := httptest.NewRecorder()
	a.Router.ServeHTTP(rr, req)
	return rr
}

func checkResponseCode(t *testing.T, expected, actual int) {
	if expected != actual {
		t.Errorf("Expected response code %d. Got %d\n", expected, actual)
	}
}

func validJSON(t *testing.T, res *bytes.Buffer) []byte {
	var jsonStr map[string]interface{}
	if err := json.NewDecoder(res).Decode(&jsonStr); err != nil {
		t.Errorf("Invalid JSON. Got '%s'", res)
	}
	rtn, err := json.Marshal(jsonStr)
	if err != nil {
		t.Errorf("Error converting JSON object back to a string.")
	}
	return rtn
}
