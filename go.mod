module macak.co.uk/goappz/core

require (
	github.com/certifi/gocertifi v0.0.0-20190410005359-59a85de7f35e // indirect
	github.com/getsentry/raven-go v0.2.0
	github.com/gorilla/mux v1.7.1
	github.com/lib/pq v1.0.0
	github.com/pkg/errors v0.8.1 // indirect
	github.com/prometheus/client_golang v0.9.2
	github.com/rs/cors v1.6.0
)
