package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"
	"time"
)

type statusWriter struct {
	http.ResponseWriter
	status int
	length int
}

func (w *statusWriter) WriteHeader(status int) {
	w.status = status
	w.ResponseWriter.WriteHeader(status)
}

func (w *statusWriter) Write(b []byte) (int, error) {
	if w.status == 0 {
		w.status = 200
	}
	w.length = len(b)
	return w.ResponseWriter.Write(b)
}

// HTTPLog | A mux middleware to log requests.
func (c *Core) HTTPLog(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		var logz *log.Logger
		start := time.Now()
		timeFormatted := time.Now().Format("02/Jan/2006 03:04:05")
		requestLine := fmt.Sprintf("%s %s %s", r.Method, r.URL, r.Proto)
		writer := statusWriter{w, 0, 0}

		next.ServeHTTP(&writer, r)

		if writer.status >= 200 && writer.status <= 299 {
			logz = c.AccessLog
		} else {
			logz = c.ErrorLog
		}

		logz.Printf(
			"%s - - [%s] \"%s\" %d %d %s\n",
			strings.Split(r.RemoteAddr, ":")[0],
			timeFormatted,
			requestLine,
			writer.status,
			writer.length,
			time.Since(start),
		)
	})
}
