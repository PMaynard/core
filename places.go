// Proccess Places

package main

import (
	"database/sql"
	"encoding/json"
	"fmt"
	"github.com/gorilla/mux"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

func validName(name string) bool {
	if len(name) < 3 {
		log.Print("[ERROR] Invalid Place Name")
		return false
	}
	return true
}

func validCountryCode(cc string) bool {
	if len(cc) != 2 {
		log.Print("[ERROR] Invalid Country Code")
		return false
	}
	return true
}

func validLocation(loc string) bool {
	// TODO: Check format of location.
	if len(strings.Split(loc, " ")) != 2 {
		log.Print("[ERROR] Invalid Location")
		return false
	}
	return true
}

// GetPlaceByID - Returns one place by ID
func (c *Core) GetPlaceByID(id string) (Place, error) {
	var p Place
	stmt, err := c.DB.Prepare(`
		SELECT placeid, name, countrycode, location_str 
		FROM places 
		WHERE PlaceID = $1`)
	if err != nil {
		return p, err
	}

	err = stmt.QueryRow(id).Scan(&p.PlaceID, &p.Name, &p.CountryCode, &p.Location)
	if err != nil {
		log.Print("[ERROR] -", err)
		return p, err

	}
	return p, nil
}

func (c *Core) places(w http.ResponseWriter, r *http.Request) {
	// Parse the request arguments
	v, err := url.ParseQuery(r.URL.RawQuery)
	if err != nil {
		log.Print("[ERROR] Unable to parse the request arguments")
		c.sendInternalError(w, r, err, http.StatusBadRequest)
		return
	}

	// Exit if we don't have required location parameter.
	if !validLocation(v.Get("location")) {
		log.Print("[ERROR] Don't have required location")
		c.sendInternalError(w, r, nil, http.StatusBadRequest)
		return
	}

	// Set the defaults
	location := v.Get("location")
	radius, err := strconv.Atoi(v.Get("radius"))
	if err != nil {
		radius = 100000 // 100km
	}

	filter := v.Get("filter")
	query := v.Get("query")

	stmt, err := c.DB.Prepare(`
		SELECT placeid, name, countrycode, location_str 
		FROM places 
		WHERE ST_DWithin(location, ST_GeomFromText('POINT(' || $1 || ')',4326), $2)
			AND CASE WHEN char_length($3) > 1 THEN countrycode=$3 ELSE true END
			AND CASE WHEN char_length($4) > 1 THEN name like '%'|| $4 || '%' ELSE true END`)
	if err != nil {
		log.Print("[ERROR] Invalid SQL")
		c.sendInternalError(w, r, err, http.StatusInternalServerError)
		return
	}

	var resData Places
	rows, err := stmt.Query(location, radius, filter, query)
	switch {
	case err == sql.ErrNoRows:
		log.Print("[ERROR]: No Results")
		c.sendInternalError(w, r, err, http.StatusNotFound)
		return
	case err != nil:
		log.Print("[ERROR] SQL Query Error")
		c.sendInternalError(w, r, err, http.StatusInternalServerError)
		return
	default:
		// Smush the response
		for rows.Next() {
			var p Place
			err = rows.Scan(&p.PlaceID, &p.Name, &p.CountryCode, &p.Location)
			if err != nil {
				log.Print("[ERROR] Parsing place from returned record")
				c.sendInternalError(w, r, err, http.StatusInternalServerError)
				return
			}
			resData = append(resData, p)
		}
	}

	// Send the respone
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(Response{Status: 1, Message: "Yo", Data: resData}); err != nil {
		panic(err)
	}
}

//  A single PLACE
func (c *Core) place(w http.ResponseWriter, r *http.Request) {
	// Parse request
	v := mux.Vars(r)

	p, err := c.GetPlaceByID(v["ID"])
	if err != nil {
		log.Print("[ERROR] Can't find place by ID")
		c.sendInternalError(w, r, err, http.StatusNotFound)
		return
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	var resData Places
	resData = append(resData, p)

	if err := json.NewEncoder(w).Encode(Response{Status: 1, Message: "Yo", Data: resData}); err != nil {
		panic(err)
	}
}

// Update a place
func (c *Core) updateplace(w http.ResponseWriter, r *http.Request) {
	// Parse request
	v := mux.Vars(r)

	var update map[string]interface{}
	if err := json.NewDecoder(r.Body).Decode(&update); err != nil {
		log.Printf("Invalid JSON. Got '%s'", r.Body)
		c.sendInternalError(w, r, err, http.StatusBadRequest)
		return
	}

	p, err := c.GetPlaceByID(v["ID"])
	if err != nil {
		c.sendInternalError(w, r, err, http.StatusNotFound)
		return
	}

	// TODO: Move this into a transaction to keep things atomic
	// TODO: Make a single statement work.. stupid thing
	for key, value := range update {
		if strings.ToLower(key) == "name" && p.Name != value {
			log.Printf("[Update] Place ID '%d' Name from '%s' to '%s'", p.PlaceID, p.Name, value)
			res, err := c.DB.Exec("UPDATE places SET name = $1 WHERE placeid = $2", value, p.PlaceID)
			if err != nil {
				c.sendInternalError(w, r, err, http.StatusInternalServerError)
				return
			}
			n, err := res.RowsAffected()
			if err != nil {
				c.sendInternalError(w, r, err, http.StatusInternalServerError)
				return
			}
			log.Printf("[Update] RowsAffected '%d'", n)
		}
		if strings.ToLower(key) == "countrycode" && p.CountryCode != value {
			log.Printf("[Update] Place ID '%d' countrycode from '%s' to '%s'", p.PlaceID, p.CountryCode, value)
			res, err := c.DB.Exec("UPDATE places SET countrycode = $1 WHERE placeid = $2", value, p.PlaceID)
			if err != nil {
				c.sendInternalError(w, r, err, http.StatusInternalServerError)
				return
			}
			n, err := res.RowsAffected()
			if err != nil {
				c.sendInternalError(w, r, err, http.StatusInternalServerError)
				return
			}
			log.Printf("[Update] RowsAffected '%d'", n)
		}
		if strings.ToLower(key) == "location" && p.Location != value {
			log.Printf("[Update] Place ID '%d' location from '%s' to '%s'", p.PlaceID, p.Location, value)
			res, err := c.DB.Exec("UPDATE places SET location = ST_GeomFromText('POINT(' || $1 || ')'), location_str = $1 WHERE placeid = $2", value, p.PlaceID)
			if err != nil {
				c.sendInternalError(w, r, err, http.StatusInternalServerError)
				return
			}
			n, err := res.RowsAffected()
			if err != nil {
				c.sendInternalError(w, r, err, http.StatusInternalServerError)
				return
			}
			log.Printf("[Update] RowsAffected '%d'", n)
		}
	}

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	if err := json.NewEncoder(w).Encode(Response{Status: 1, Message: fmt.Sprintf("Place ID '%d' Successfully Updated.", p.PlaceID), Data: nil}); err != nil {
		panic(err)
	}
}

// Update a place
func (c *Core) createplace(w http.ResponseWriter, r *http.Request) {
	var p Place
	if err := json.NewDecoder(r.Body).Decode(&p); err != nil {
		log.Printf("Invalid JSON. Got '%s'", r.Body)
		c.sendInternalError(w, r, err, http.StatusBadRequest)
		return
	}

	if !validName(p.Name) {
		c.sendInternalError(w, r, nil, http.StatusBadRequest)
		return
	}

	if !validLocation(p.Location) {
		c.sendInternalError(w, r, nil, http.StatusBadRequest)
		return
	}

	if !validCountryCode(p.CountryCode) {
		c.sendInternalError(w, r, nil, http.StatusBadRequest)
		return
	}

	// log.Printf("[Create] Place ID '%d' location from '%s' to '%s'", p.PlaceID, p.Location, value)
	var id int
	err := c.DB.QueryRow(`
		INSERT INTO places 
			(name, countryCode, location, location_str) 
			VALUES ($1, $2, ST_GeomFromText('POINT(' || $3 || ')'), $3)
			RETURNING PlaceID`, p.Name, p.CountryCode, p.Location).Scan(&id)
	if err != nil {
		c.sendInternalError(w, r, err, http.StatusInternalServerError)
		return
	}

	log.Printf("[Create] Inserted new Place ID '%d')", id)

	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(http.StatusOK)

	var resData Places
	p.PlaceID = id
	resData = append(resData, p)

	if err := json.NewEncoder(w).Encode(Response{Status: 1, Message: "Successfully Added Place", Data: resData}); err != nil {
		panic(err)
	}
}
